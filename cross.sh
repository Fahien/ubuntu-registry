CI_REGISTRY=registry.gitlab.collabora.com
CI_REGISTRY_IMAGE=$CI_REGISTRY/fahien/ubuntu-registry

docker login $CI_REGISTRY
docker buildx create --name cross-builder --use
docker buildx inspect --bootstrap
docker buildx build --platform linux/amd64,linux/arm64 -t $CI_REGISTRY_IMAGE:latest --push .

