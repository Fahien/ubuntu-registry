FROM ubuntu:rolling
RUN apt-get update && \
	apt-get -y upgrade && \
	apt-get -y install python3 linux-libc-dev zlib1g-dev git pkg-config libwayland-dev cmake meson clang
